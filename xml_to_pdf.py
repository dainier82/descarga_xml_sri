import sys
import json
import datetime
from suds.client import Client
from lxml import etree
import re
import xml.etree.ElementTree as ET
import sys, os
import barcode
from barcode.writer import ImageWriter

def xml_to_obj(file_name):
    """
    Create object from xml
    :param file_name: File name
    :return XML object
    """
    with open(file_name, 'r') as myfile:
        data = myfile.read()
        data = data.replace('&lt;', '<')
        data = data.replace('&gt;', '>')
        data = data.replace('&quot;', '"')
        data = data.replace('<![CDATA[', '')
        data = data.replace(']]>', '')

    _estado = re.search('<estado>(.*)</estado>', data)
    _numeroAutorizacion = re.search('<numeroAutorizacion>(.*)</numeroAutorizacion>', data)
    _fechaAutorizacion = re.search('<fechaAutorizacion>(.*)</fechaAutorizacion>', data)
    #print(_numeroAutorizacion.group(1))

    sheet_index = data.find('<?xml')
    while sheet_index >= 0:
        last_index = data.find('?>')
        if last_index < 0:
            last_index = sheet_index + 5
        data = data[:sheet_index] + data[last_index + 2:]
        sheet_index = data.find('<?xml')

    # if data.find('<comprobante>') >= 0:
    #     data = data.split("<comprobante>", 1)[1]
    #     data = data.split("</comprobante>", 1)[0]
    # if data.find('<ds') >= 0:
    #     data = data.split("<ds", 1)[0] +"</factura>"
    # sheet_index = data.find('<detallesAdicionales>')
    # while sheet_index >= 0:
    #     last_index = data.find('</detallesAdicionales>')
    #     if last_index < 0:
    #         last_index = sheet_index + 21
    #     data = data[:sheet_index] + data[last_index + 22:]
    #     sheet_index = data.find('<detallesAdicionales>')
    # sheet_index = data.find('<infoAdicional>')
    # print('sheet_index', sheet_index)
    # while sheet_index >= 0:
    #     last_index = data.find('</infoAdicional>')
    #     if last_index < 0:
    #         last_index = sheet_index + 15
    #     data = data[:sheet_index] + data[last_index + 16:]
    #     sheet_index = data.find('<infoAdicional>')

    xml = etree.fromstring(data)
    xml_obj = {}
    tmp = {}
    xml_obj['infoTributaria'] = {}
    tmp['totalImpuesto'] = {}
    tmp['pago'] = {}
    tmp['tmp_pago'] = []
    tmp['impuesto'] = {}
    tmp['tmp_impuesto'] = []
    tmp['detalle'] = {}
    tmp['totalImp'] = []
    # infoTributaria
    for log in xml.iter('infoTributaria'):
        for l in log:
            xml_obj['infoTributaria'].update({
                l.tag: l.text})
    # infoFactura
    xml_obj['infoFactura'] = {}
    for log in xml.iter('infoFactura'):
        for l in log:
            if l.tag == 'totalConImpuestos':
                for log in l.iter('totalImpuesto'):
                    for l in log:
                        tmp['totalImpuesto'].update({l.tag: l.text})
                    tmp['totalImp'].append(tmp['totalImpuesto'])
                    tmp['totalImpuesto'] = {}

                xml_obj['infoFactura'].update({
                    'totalConImpuestos': tmp['totalImp']})
                tmp['totalImp'] = []

            if l.tag == 'pagos':
                for log in xml.iter('pago'):
                    for l in log:
                        tmp['pago'].update({l.tag: l.text})

                    tmp['tmp_pago'].append(tmp['pago'])

                xml_obj['infoFactura'].update({'pagos': tmp['tmp_pago']})
            else:
                xml_obj['infoFactura'].update({l.tag: l.text})
    # Details
    xml_obj['detalles'] = {}
    tmp['tmp_detalles'] = []
    for log in xml.iter('detalle'):
        for l in log:
            if l.tag == 'impuestos':
                for log in l.iter('impuesto'):
                    for l in log:
                        tmp['impuesto'].update({l.tag: l.text})
                    tmp['tmp_impuesto'].append(tmp['impuesto'])
                    tmp['impuesto'] = {}

                tmp['detalle'].update({'impuesto': tmp['tmp_impuesto']})
                tmp['tmp_impuesto'] = []
            else:
                tmp['detalle'].update({l.tag: l.text})

        tmp['tmp_detalles'].append(tmp['detalle'])
        tmp['detalle'] = {}
    xml_obj['detalles'].update({'detalle': tmp['tmp_detalles']})
    # Others
    xml_obj['otrosRubrosTerceros'] = {}
    others = 0.00
    for log in xml.iter('otrosRubrosTerceros'):
        for l in log:
            if l.tag == 'rubro':
                for logx in l.iter('rubro'):
                    for l1 in logx:
                        if l1.tag == 'total':
                            others += float(l1.text)
    xml_obj['otrosRubrosTerceros'].update({'total':others})
    # infoAdicional
    xml_obj['infoAdicional'] = {}
    for log in xml.iter('infoAdicional'):
        for l in log:
            if 'nombre' in l.attrib:
                xml_obj['infoAdicional'].update({l.attrib['nombre']: l.text})
    
    xml_obj['autorizacion'] = {}
    xml_obj['autorizacion'].update({'estado': _estado.group(1)})
    xml_obj['autorizacion'].update({'numeroAutorizacion': _numeroAutorizacion.group(1)})
    xml_obj['autorizacion'].update({'fechaAutorizacion': _fechaAutorizacion.group(1)})
    
    return xml_obj

def build_barcode(code):
    try:
        barcode_format = barcode.get_barcode_class('code128')
        _barcode = barcode_format(code, writer=ImageWriter())
        _barcode.default_writer_options['write_text'] = False
        _barcode.save(code + 'png')

    except Exception as e:
        print('get_img_barcode', e)
    
def build_pdf(data):
    """
    """
    barcode = build_barcode('123')
    codDoc = {'01':'FACTURA',
              '03':'LIQUIDACI&Oacute;N DE COMPRA',
              '04':'NOTA DE CR&Eacute;DITO',
              '05':'NOTA DE D&Eacute;BITO',
              '06':'GU&Iacute;A DE REMISI&Oacute;N',
              '07':'COMPROBANTE DE RETENCI&Oacute;N',
              }
    ambiente = {'1':'PRUEBAS', '2':'PRODUCCI&Oacute;N'}
    tipoEmision = {'1':'NORMAL'}
    formapago = {'01': 'SIN UTILIZACION DEL SISTEMA FINANCIERO',
                 '15': 'COMPENSACI&Oacute;N DE DEUDAS',
                 '16': 'TARJETA DE D&Eacute;BITO',
                 '17': 'DINERO ELECTR&Oacute;NICO',
                 '18': 'TARJETA PREPAGO',
                 '19': 'TARJETA DE CR&Eacute;DITO',
                 '20': 'OTROS CON UTILIZACI&Oacute;N DEL SISTEMA FINANCIERO',
                 '21': 'ENDOSO DE T&Iacute;TULOS'
                }
    porcentaje_iva = {'0': '0%',
                '2': '12%',
                '3': '14%',
                '6': 'No Objeto de Impuesto',
                '7': 'Exento de IVA',
                '8': 'IVA diferenciado',
            }
    number = data['infoTributaria']['estab'] + '-' + data['infoTributaria']['ptoEmi'] + '-' + data['infoTributaria']['secuencial']

    body_table = """
        <table class="" align="center" width="100%" high="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td width="48%" class="all_border" style="border-radius: 10px;">
                    <br /><br />
                    <h1 style="color:red; text-align: center;">SIN LOGO</h1>
                    <br /><br />
                    <div style="text-align: center;"> {}</div>
                    <br />
                    <div> Direcci&oacute;n Matr&iacute;z: {}</div>
                    <div> Direcci&oacute;n Sucursal: {}</div>
                    <div> Contribuyente Especial Nro: {}</div>
                    <div> Obligado A Llevar Contabilidad: {}</div>
                </td>
                <td width="2%"></td>
                <td width="48%" class="all_border" rowspan="2" style="border-radius: 10px;">
                    <h3 style="text-align: center;">R.U.C.: {}</h3>
                    <h3 style="text-align: center;"><b>{}</b></h3>
                    <div style="text-align: center;">No. {}</div>
                    <br /><br />
                    <div style="text-align: center;"><b>N&Uacute;MERO DE AUTORIZACI&Oacute;N</b></div>
                    <div style="text-align: center;">{}</div>
                    <br /><br />
                    <div style="text-align: center;">FECHA Y HORA DE AUTORIZACI&Oacute;N: {}</div>
                    <div style="text-align: center;">AMBIENTE: {}</div>
                    <div style="text-align: center;">EMISI&Oacute;N: {}</div>
                    <br />
                    <div style="text-align: center;"><b>CLAVE DE ACCESO</b></div>
                    <div style="text-align: center;">
                        <span style="font-family: 'Libre Barcode 39 Extended Text'; font-size: 18px;">{}</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="all_border" style="border-radius: 10px;">
                    <div style="text-align: center;">Fecha Emisi&oacute;n: {}</div>
                    <div style="text-align: center;">C&eacute;dula/RUC: {}</div>
                    <div style="text-align: center;">Raz&oacute;n Social: {}</div>
                    <div style="text-align: center;">Direcci&oacute;n: {}</div>
                </td>
            </tr>
        </table>
    """.format(data['infoTributaria']['razonSocial'].encode('utf8').decode('utf8'),
               data['infoTributaria']['dirMatriz'].encode('utf8').decode('utf8'),
               data['infoFactura']['dirEstablecimiento'].encode('utf8').decode('utf8') if data['infoFactura'].get('dirEstablecimiento', False) else '',
               data['infoFactura']['contribuyenteEspecial'] if data['infoFactura'].get('contribuyenteEspecial', False) else '---',
               data['infoFactura']['obligadoContabilidad'] if data['infoFactura'].get('obligadoContabilidad', False) else 'NO',
               #
               data['infoTributaria']['ruc'],
               codDoc[data['infoTributaria']['codDoc']],
               number,
               data['infoTributaria']['claveAcceso'],
               data['autorizacion']['fechaAutorizacion'],
               ambiente[data['infoTributaria']['ambiente']],
               tipoEmision[data['infoTributaria']['tipoEmision']],
               data['infoTributaria']['claveAcceso'],
               #
               data['infoFactura']['fechaEmision'],
               data['infoFactura']['identificacionComprador'],
               data['infoFactura']['razonSocialComprador'].encode('utf8').decode('utf8'),
               data['infoFactura']['direccionComprador'].encode('utf8').decode('utf8') if data['infoFactura'].get('direccionComprador', False) else '---',
               )

    _lines = ''
    for line in data['detalles']['detalle']:
        _lines += """
            <tr>
                <td>{}</td>
                <td>{}</td>
                <td style="text-align: right;">{}</td>
                <td style="text-align: right;">{}</td>
                <td style="text-align: right;">{}</td>
                <td style="text-align: right;">{}</td>
            </tr>
        """.format(line['codigoPrincipal'],
                   line['descripcion'],
                   line['cantidad'],
                   line['precioUnitario'],
                   line['descuento'],
                   line['precioTotalSinImpuesto'])

    body_table += """
        <br />
        <table class="all_border">
            <tr>
                <th style="text-align: center;">C&Oacute;DIGO</th>
                <th style="text-align: center;">DESCRIPCI&Oacute;N</th>
                <th style="text-align: center;">CANTIDAD</th>
                <th style="text-align: center;">PRECIO</th>
                <th style="text-align: center;">DESCUENTO</th>
                <th style="text-align: center;">TOTAL</th>
            </tr>
            {}
        </table>
    """.format(_lines)

    _info_lines = ''
    for k, v in data['infoAdicional'].items():
        _info_lines += """
            <tr>
                <td>{}</td>
                <td>{}</td>
            </tr>
        """.format(k.encode('ascii', 'ignore').decode('ascii'), 
                   v.encode('ascii', 'ignore').decode('ascii'))

    _formapago_lines = ''
    for line in data['infoFactura']['pagos']:
        _formapago_lines += """
            <tr>
                <td>{}</td>
                <td style="text-align: right;">{}</td>
                <td style="text-align: right;">{}</td>
                <td>{}</td>
            </tr>
        """.format(formapago[line['formaPago']], 
            line['total'], 
            line['plazo'] if line.get('plazo', False) else '', 
            line['unidadTiempo'] if line.get('unidadTiempo', False) else '', 
        )  

    
    porcentaje_iva_lines = ''
    for line in data['infoFactura']['totalConImpuestos']:
        porcentaje_iva_lines += """
            <tr>
                <td>Subtotal {}</td>
                <td style="text-align: right;">{}</td>
            </tr>
        """.format(porcentaje_iva[line['codigoPorcentaje']], 
            line['baseImponible'], 
        ) 

    body_table += """
        <br />
        <table class="" align="center" width="100%" high="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td width="70%" class="">
                    <table class="all_border" width="100%">
                        <tr style="background-color: #92a8d1;"><th colspan="2"><div style="text-align: center;"><b>Informaci&oacute;n Adicional</b></div></th></tr>
                        {}
                    </table>
                    <br />
                    <table class="all_border" width="100%">
                        <tr>
                            <th style="text-align: center;">Forma de Pago</th>
                            <th style="text-align: center;">Valor</th>
                            <th style="text-align: center;">Plazo</th>
                            <th style="text-align: center;">Tiempo</th>
                        </tr>
                        {}
                    </table>                    
                </td>
                <td width="30%" class="" style="vertical-align:top">
                    <table class="all_border" width="100%">
                        {}
                        <!-- -->
                        <tr>
                            <td>SUBTOTAL SIN IMPUESTOS</td>
                            <td style="text-align: right;">{}</td>
                        </tr>
                        <tr>
                            <td>DESCUENTO</td>
                            <td style="text-align: right;">{}</td>
                        </tr>
                        <tr>
                            <td>IVA</td>
                            <td style="text-align: right;">{}</td>
                        </tr>
                        <tr>
                            <td>PROPINA</td>
                            <td style="text-align: right;">{}</td>
                        </tr>
                        <tr>
                            <td>VALOR TOTAL</td>
                            <td style="text-align: right;">{}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    """.format(
        _info_lines,
        _formapago_lines,
        porcentaje_iva_lines,
        data['infoFactura']['totalSinImpuestos'],
        data['infoFactura']['totalDescuento'],
        data['infoFactura']['valor'],
        data['infoFactura']['propina'],
        data['infoFactura']['importeTotal'],
    )

    body = """
        <html>  
        <head>
            <meta charset="utf-8">
            <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
            <link href="https://fonts.googleapis.com/css2?family=Libre+Barcode+39+Extended+Text&display=swap" rel="stylesheet">

            <script>
                
            </script>
        </head>  
        <body>  
            {}
        </body>  
        </html>  
    """.format(body_table)


    import pdfkit
    #sudo apt-get install wkhtmltopdf
    #pip install pdfkit

    pdf = pdfkit.from_string(body.encode('utf8').decode('utf8'), 'out.pdf', css='style.css')    

def app():
    """
    Descargar xml del SRI
    chmod +x ./xml_to_pdf.py
    python2 ./xml_to_pdf.py
    """
    #path = 'FACTURA_017-012-000028912.xml'
    #path = 'FAC-020-908-000334546.xml'
    path = 'Factura 001-001-000000014.xml'
    data = xml_to_obj(path)
    print(data)

    build_pdf(data)

if __name__ == '__main__':
    app()