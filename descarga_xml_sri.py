import sys
import json
import datetime
from suds.client import Client

url = 'https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl'
#key = '0507202301179123341700120010140078626521438912216'

def app():
    """
    Descargar xml del SRI
    chmod +x ./descarga_xml_sri.py
    python2 ./descarga_xml_sri.py
    """
    #Cargar listado de clave de acceso
    with open('datos.txt','r') as myfile:
        lines = myfile.readlines()
        for line in lines:
            print(line)
            arr = line.split("\t")
            key = arr[9]
            
            #Descargar XML
            client = Client(url)
            response = client.service.autorizacionComprobante(key)
            numeroComprobantes = int(response['numeroComprobantes'])
            swfile = open(key + '.xml','wb')
            swfile.write(str(response))
            swfile.close()

    print('***Proceso Terminado***')

if __name__ == '__main__':
    app()